using static System.Math;
using static System.Console;

class math{
	static void Main(){
	
		double sqrt2;
		sqrt2=Sqrt(2);
		double pinumber=PI;
		double enumber=E;
		double exppi=Exp(pinumber);
		double minus=-1.0;
		Write($"sqrt(2) = {sqrt2}\n");
		Write($"exp(pi) = {exppi}\n");
		Write($"pi^e={Pow(pinumber,enumber)} \n");
		Write($"sqrt(2)sqrt(2) = {sqrt2*sqrt2} \n");
		Write($"exp(pi)exp(-pi) = {exppi*Pow(exppi,minus)}\n");
		Write($"pi^e x pi^-e ={Pow(pinumber,enumber)*Pow(pinumber,-enumber)}\n");
	}
}




