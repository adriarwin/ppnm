using static System.Console;
using static System.Math;

class main{
	static void Main(){
		complex z= new complex(3,4) ;
		complex t= new complex(2,4) ;
		
		//Exercise. Cmath class.
		//sqrt(-1)
		complex m= new complex(-1,0) ;
		complex ei;
		complex epi;
		complex impi= new complex(0,PI);
		complex mone;
		complex none;
		complex ii;
		complex logi;
		complex sinimpi;
		mone=cmath.sqrt(m);
		none=cmath.sqrt(cmath.I);
		ei=cmath.exp(cmath.I);
		epi=cmath.exp(impi);
		ii=(cmath.I).pow(cmath.I);
		logi=cmath.log(cmath.I);
		sinimpi=cmath.sin(impi);
		Write("sqrt(-1)={0}+{1}i. Should be i.\n",mone.Re,mone.Im);
		Write("sqrt(i)={0}+{1}i. Should be cos(pi/4)*(1+i). Cos(pi/4)={2}\n",none.Re,none.Im,Cos(PI/4));
		Write("e^i={0}+{1}i. Should be Cos(1)+iSin(1). Cos(1)={2}. Sin(1)={3}.\ne^ipi={4}+{5}i. ",ei.Re,ei.Im,Cos(1),Sin(1),epi.Re,epi.Im);
		Write("Should be -1.\n");
		Write("i^i={0}+{1}i. Should be e^-PI/2={2}\n",ii.Re,ii.Im,Exp(-PI/2));
		Write("log(i)={0}+{1}i. Should be PI/2\nsin(pii)={2}+{3}i\n",logi.Re,logi.Im,sinimpi.Re,sinimpi.Im);
		}
}
		
