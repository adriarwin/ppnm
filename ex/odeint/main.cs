using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;

class main{

	static void Main(){
		System.Func<double,vector,vector> pend=delegate(double t, vector y){
			double e=0.25;
			double f=5;
			vector dydt=new vector(y[1],-e*y[1]-f*Sin(y[0]));
			return dydt;};
		vector y0=new vector(PI-0.1,0.0);
		double ini=0.0;
		double fin=10.0;
		var xlist = new List<double>();
		var ylist = new List<vector>();
		vector result=ode.ivp(pend,ini,y0,fin,xlist,ylist);
		for (int i=0;i<xlist.Count;i++)
			WriteLine($"{xlist[i]} {ylist[i][0]} {ylist[i][1]}");
				
	}





}
