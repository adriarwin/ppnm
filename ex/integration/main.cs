using static System.Math;
using static System.Console;
using System;

class main{
	public static double erf(double z){
		Func<double,double> f = x => Exp(-x*x);
		return integrate.quad(f,0,z)*2/Sqrt(PI);
		}
	static void Main(){
		Func<double,double> f = delegate(double x){return Log(x)/Sqrt(x);};
		double result = integrate.quad(f,0,1);
		Write($"Numerical result of the integral: {result}.\nExact result:-4\n");
		Write("\n\n");
		var outfile = new System.IO.StreamWriter("data.txt");
		double a=-3;
		double b=3;
		double N=100;
		double dx=(b-a)/N;
		for(double x=a;x<=b;x+=dx){
			outfile.WriteLine($"{x} {erf(x)}");
			}	
		outfile.Close();


	}

}
