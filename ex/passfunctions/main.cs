using static System.Math;
using static System.Console;
using System;
class main{
	static void Main(){
		//Diferent ways of doing this
		double a=0;
		double b=PI;
		double k=1;
		double dx=(b-a)/10.0;
		WriteLine($"a={a},b={b}");
		Func<double,double> sink=delegate(double x){return Sin(k*x);};
		for(double l=1;l<=3;l+=1){
		WriteLine($" x, Sin({k}x)");
		table.make_table(sink,a,b,dx);
		k+=1;
		}	
	}
}
