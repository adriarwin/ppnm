using static System.Console;
using System.Threading;
using System;


class main{
	public class data {public int a,b; public double sum;}

	public static void harmonic_sum(object obj){
		data x= (data)obj;
		x.sum= 0; for(int i=x.a;i <= x.b;i++) x.sum +=1.0/i;
		}


	public static void Main(){
		int N=10000;
		data x = new data();
		x.a=1; x.b=N/2;
		data y = new data();
		y.a=N/2; y.b=N;
		Thread t = new Thread(harmonic_sum);
		Thread s = new Thread(harmonic_sum);
		t.Start(x);
		s.Start(y);
		t.Join();
		s.Join();
		WriteLine($"harmonic sum from {x.a} to {x.b} equals {x.sum}. Processor 1.");
		WriteLine($"harmonic sum from {y.a} to {y.b} equals {y.sum}. Processor 2.");
		WriteLine($"harmonic sum from {x.a} to {y.b} equals {x.sum+y.sum}");

	}
}
