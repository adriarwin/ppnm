using static System.Console;
using static System.Math;

public static class function{
		public static bool approx (double a, double b, double tau=1e-9, double epsilon=1e-9){
			if (Abs(a-b)<tau){
			return true;}
			else if (Abs(a-b)/(Abs(a)+Abs(b))<epsilon){
			return true;}
			else{return false;}			

		}

		

}


class epsilon{
	static void Main(){
		//Double and Float Machine epsilon+Max and min int value
		int i=1; while(i+1>i){i++;}
		Write("Manually max int {0}\nTheoretical max int {1}\n",i, int.MaxValue);
		int t=1; while(t-1<i){t--;}
		Write("Manually min int {0}\nTheoretical min int {1}\n\n",t, int.MinValue);
		double x=1; while(1+x!=1){x/=2;} x*=2;
		float y=1F; while((float)(1F+y) !=1F){y/=2F;} y*=2F;
		double xt=Pow(2,-52);
		double yt=Pow(2,-23);
		Write("For Double Numbers:\nManual machine epsilon {0}\nTheoretical machine epsilon{1}\n\n",x,xt);
		Write("For Float Numbers\nManual machine epsilon {0} \nTheoretical machine epsilon{1}\n\n",y,yt);
		
		double sumA=1;
		double sumB=0;
		double tiny=xt/2;
		int n=(int)1e6;
	
		for (int s=0;s<n;s++){sumA+=tiny;};
		for (int l=0;l<n;l++){sumB+=tiny;};
		sumB+=1;
		Write ($"sumA-1={sumA-1:e} should be {n*tiny:e}\n");
		Write ($"sumB-1={sumB-1:e} should be {n*tiny:e}\n");
                Write ("Memory is already occupied by the 1 in the sumA case, such that it can not notice epsilon\n\n");
		//Lets test the function.
		double taue=1e-9;
		double a=1;
                double b=1;
		Write ("Test of the bool function\n");
		
		Write ($"a=1,b=1,Result:{function.approx(a,b)}\n");
		Write ($"a=1,b=1+2tau,Result:{function.approx(a,b+2*taue)}\n");
		int cont=1;
		while(function.approx(a,b)^false==true){
		b+=taue;
		cont+=1;};
		Write ($"a=1, b=1+{cont}tau, Result:{function.approx(a,b)}\n");
		}	

}
