NAME: Adrià Bravo Vidal.
STUDENT NUMBER: 202102106.
I have implemented a Bilinear interpolator via a class defined in the bl.cs file. Given a sample of data organized in a 
rectiliniar grid, nx x ny, there are nx-1 x ny-1 grid rectangles which we can denote by (i,j) inside which we can define an 
interpolation function B_ij(x,y)= a_ij + b_ij x + c_ij y + d_ij x y. The coefficients a, b, c, d are (nx-1) x (ny-1) matrixes
which we found by imposing B_ij(x,y) to be equal to the four known sample values at the corner of each grid rectangle. 
The class bl.cs finds the matrix coefficients and use them to calculate the interpolated function of the sample at each point 
of the space delimited by the sample.

To test the class, I have used four different functions which give obvious results for the a,b,c and d matrixes. 
These are f0(x)=1, f1(x)=x, f2(x)=y, f3(x)=xy. Using the approx method from matrix.cs, I have verified that the 
expected a,b,c and d matrixes for each of the functions are the same as the ones given by our class for a irregular 
random rectiliniar grid . The positive results can be seen at ‘out.txt’.
      
For further testing, I have generated two samples of a Guassian matrix centred at (1,1) with a rectiliniar 12x12 gird for the 
case in which the grid is regular and irregular. I have interpolated the samples with my class and represented both cases 
in 2D maps (which represents the value of the function at each (x,y) point with a color scale). Results can be seen at 
“gauss.png” and “irrgauss.png”.
 
Finally, I have estimeted the error we do when we interpolate the same Gaussian function for rectangular regular grids of 
different sizes, NxN. To estimate the error, I have calculated the absoulte difference between B(x,y) and the real value 
of the function through all the used space, which is equivalent to integrate Abs(B(x,y)-f(x,y)) for all the surface covered 
by the sample.
 
Personal evaluation: I have succesfully completed what the problem demands for. To make sure of it, I have done careful 
tests, and generated graphs which show it. I have also estimated the error of our interpolation function. I would say that 
the work done corresponds roughly to complete the part A and B of a homework, I’d give my work a 9/10 rating. 

