using static System.Console;
using static System.Math;
using System;


public class bl{
        public double[] x,y;
        public matrix F,a,b,c,d;

        public void blc(double[] xs,double[] ys,matrix Fs){
                x=xs;
                y=ys;
                F=Fs;
                int nx=x.Length;
                int ny=y.Length;
                a=new matrix(nx-1,ny-1);
                b=new matrix(nx-1,ny-1);
                c=new matrix(nx-1,ny-1);
                d=new matrix(nx-1,ny-1);
                vector keeper=new vector(4);
                //Now we follow with a loop.
                for(int i=0;i<nx-1;i++){
                        for(int j=0;j<ny-1;j++){
                                //Now we gotta find the 4 coefficients for that grid.
                                keeper=abcd(i,j);
                                a[i,j]=keeper[0];
                                b[i,j]=keeper[1];
                                c[i,j]=keeper[2];
                                d[i,j]=keeper[3];
                          };}

        }
        public vector abcd(int i,int j){
                matrix Amat=new matrix(4,4);
                vector Fvec=new vector(4);
                vector result=new vector(4);
                double x1=x[i];
                double y1=y[j];
                double x2=x[i+1];
                double y2=y[j+1];
                double constant=1/((x2-x1)*(y2-y1));
                Amat[0]=new vector(x2*y2,-y2,-x2,1);
                Amat[1]=new vector(-x2*y1,y1,x2,-1);
                Amat[2]=new vector(-x1*y2,y2,x1,-1);
                Amat[3]=new vector(x1*y1,-y1,-x1,1);
                Amat=constant*Amat;
                Fvec[0]=F[i,j];
                Fvec[1]=F[i,j+1];
                Fvec[2]=F[i+1,j];
                Fvec[3]=F[i+1,j+1];

                result=Amat*Fvec;
		return result;
        }

        public static int binsearch(double[] xs, double z)
        {/* locates the interval for z by bisection */
        if(!(xs[0]<=z && z<=xs[xs.Length-1])) throw new Exception("binsearch: bad z");
        int i=0, j=xs.Length-1;
        while(j-i>1){
                int mid=(i+j)/2;
                if(z>xs[mid]) i=mid; else j=mid;
                }
        return i;
        }

        public double bilinear(double px,double py){
                int i=binsearch(x,px);
                int j=binsearch(y,py);
                double result=a[i,j]+b[i,j]*px+c[i,j]*py+d[i,j]*px*py;
                return result;
                }


}
