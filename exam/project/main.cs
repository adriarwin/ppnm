using static System.Console;
using static System.Math;
using System;

class main{
	static void Main(){
		//The tests start! 
		//Test one. f(x,y)=x+y
		//We expect b[i,j],c[i,j]=1 for al i and j. Frist, regular grid.
		Func<vector,double> f0=delegate(vector x0){
				return 1.0;};
		Func<vector,double> f1=delegate(vector x1){
				return x1[0];};
		Func<vector,double> f2=delegate(vector x2){
				return x2[1];};
		Func<vector,double> f3=delegate(vector x3){
				return x3[0]*x3[1];};
		Random random = new Random();
		
		int n=10;
		double[] xtest1=new double[n];
		double[] ytest1=new double[n];
		matrix At0=new matrix(n,n);
		matrix At1=new matrix(n,n);
		matrix At2=new matrix(n,n);
		matrix At3=new matrix(n,n);
		for(int i=0;i<n;i++){xtest1[i]=0.0+random.NextDouble()*10;
					ytest1[i]=0.0+random.NextDouble()*10;}
		Array.Sort(xtest1);
		Array.Sort(ytest1);
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
			vector vtest=new vector(xtest1[i],ytest1[j]);
			At0[i,j]=f0(vtest);
			At1[i,j]=f1(vtest);
			At2[i,j]=f2(vtest);
			At3[i,j]=f3(vtest);	
			}}		
		bl f0c=new bl();
		bl f1c=new bl();
		bl f2c=new bl();
		bl f3c=new bl();

		f0c.blc(xtest1,ytest1,At0);
		f1c.blc(xtest1,ytest1,At1);
		f2c.blc(xtest1,ytest1,At2);
		f3c.blc(xtest1,ytest1,At3);

		matrix onem=new matrix(n-1,n-1);
		matrix zerom=new matrix(n-1,n-1);

		for(int i=0;i<n-1;i++){
			for(int j=0;j<n-1;j++){
				onem[i,j]=1;
				zerom[i,j]=0;
				};}
		
		Write("Our interpolator is made of B(x,y)=a+bx+cy+dxy. We have, for each coefficient, a (m-1)x(n-1) matrix where\n ");
		Write("m corresponds to the number of x lines we have and n to the number of n lines we have. To test our function\n");
		Write("we use four different functions: f0=1,f1=x,f2=y,f3=xy. For each of those, we expect a,b,c,d (respectively) \n ");
		Write("to be a full of ones matrix and the rest a full of zeros matrix. We test that for each matrix using the approx \n ");
		Write("function from matrix.cs. We get: \n\n");

		WriteLine("Matrix a, b, c, d equal to the expected value. True printed if our expectation is satisfied.\n");
		WriteLine($"f0: {(f0c.a).approx(onem)},{(f0c.b).approx(zerom)},{(f0c.c).approx(zerom)},{(f0c.d).approx(zerom)}");
		WriteLine($"f1: {(f1c.a).approx(zerom)},{(f1c.b).approx(onem)},{(f1c.c).approx(zerom)},{(f1c.d).approx(zerom)}");
		WriteLine($"f2: {(f2c.a).approx(zerom)},{(f2c.b).approx(zerom)},{(f2c.c).approx(onem)},{(f2c.d).approx(zerom)}");
		WriteLine($"f3: {(f3c.a).approx(zerom)},{(f3c.b).approx(zerom)},{(f3c.c).approx(zerom)},{(f3c.d).approx(onem)}");

		Func<vector,double> gs=delegate(vector x4){
			return (1/Sqrt(2*PI))*Exp(-0.5*((x4[0]-1)*(x4[0]-1)+(x4[1]-1)*(x4[1]-1)));}; 
		
		n=12;		
		double[] xtest2=new double[n];
                double[] ytest2=new double[n];
		double[] xtest3=new double[n];
		double[] ytest3=new double[n];

		xtest2[0]=-5;
		ytest2[0]=-5;
		xtest2[n-1]=5;
		ytest2[n-1]=5;

		xtest3[0]=-5;
                ytest3[0]=-5;
                xtest3[n-1]=5;
                ytest3[n-1]=5;
		
		for(int i=1;i<n-1;i++){xtest2[i]=-4.99+random.NextDouble()*9.99;
                                       ytest2[i]=-4.99+random.NextDouble()*9.99;}
		for(int i=1;i<n-1;i++){xtest3[i]=-5+10*(i*1.0)/(n-1);
					ytest3[i]=-5+10*(i*1.0)/(n-1);}

		var outfile1=new System.IO.StreamWriter("data1.txt");
		var outfile2=new System.IO.StreamWriter("data2.txt");
		Array.Sort(xtest2);
		Array.Sort(ytest2);
		Array.Sort(xtest3);
		Array.Sort(ytest3);
		matrix At4=new matrix(n,n);
		matrix At5=new matrix(n,n);
                for(int i=0;i<n;i++){
                        for(int j=0;j<n;j++){
                        vector vtest=new vector(xtest2[i],ytest2[j]);
			vector vtest1=new vector(xtest3[i],ytest3[j]);
                        At4[i,j]=gs(vtest);
			At5[i,j]=gs(vtest1);
			outfile2.WriteLine($"{xtest3[i]} {ytest3[j]} {At5[i,j]}");
			outfile1.WriteLine($"{xtest2[i]} {ytest2[j]} {At4[i,j]}");}}
		outfile1.Write("\n\n");
		outfile2.Write("\n\n");
		bl f4c=new bl();
		bl f5c=new bl();
                f4c.blc(xtest2,ytest2,At4);
		f5c.blc(xtest3,ytest3,At5);

		double linf=-5.0;
		double lsup=5.0;
		double Ncount=50;
		double dx=(lsup-linf)/Ncount;
		
		for(double x=linf;x<lsup;x+=dx){
			for(double y=linf;y<lsup;y+=dx){
						outfile1.WriteLine($"{x} {y} {f4c.bilinear(x,y)}");
						outfile2.WriteLine($"{x} {y} {f5c.bilinear(x,y)}");};}
		outfile1.Close();					
		outfile2.Close();

		int linf1=5;
                int lsup1=21;
                
                double counter=0.0;
                //Integration parameters.
                double dxint=0.001;
                double linfint=-5;
                double lsupint=5;
                var outfile3=new System.IO.StreamWriter("data3.txt");
                //For the number of lines in the grid.
                for(int p=linf1;p<=lsup1;p+=2){
                        counter=0;
                        double[] xtest5=new double[p];
                        double[] ytest5=new double[p];
                        matrix At6=new matrix(p,p);
                        xtest5[0]=linfint;
                        xtest5[p-1]=lsupint;
                        ytest5[0]=linfint;
                        ytest5[p-1]=lsupint;

			for(int i=1;i<p-1;i++){xtest5[i]=-5+10*(i*1.0)/(p-1);
                                        ytest5[i]=-5+10*(i*1.0)/(n-1);}

               	        for(int i=0;i<p;i++){
                               for(int j=0;j<p;j++){
                                      vector vtest=new vector(xtest5[i],ytest5[j]);
                                      At6[i,j]=gs(vtest);};}
                         bl f6c=new bl();
                         f6c.blc(xtest5,ytest5,At6);
                         for(double xs=linfint;xs<lsupint;xs+=dxint){
                         for(double ys=linfint;ys<lsupint;ys+=dxint){
                                    vector vtest=new vector(xs,ys);
                                     counter+=Abs((f6c.bilinear(xs,ys)-gs(vtest)));};}

                        outfile3.WriteLine($"{p} {counter}");}

                 outfile3.Close();




	}





}
