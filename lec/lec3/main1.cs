
using System;
using static System.Console;

class main{

public static void Main(){//Delegate, a function you can pass around.
	System.Func<double> fun = delegate(){return 7;}
	Func<double.double> square = delegate(double x){return x*x};
	Action hello = delegate() { WriteLine("hello");
	hello();
	WriteLine($"fun()={fun()} should be equal 7");
	WriteLine($"square(2)={square(2)} should be equal 4");
	
}
}
