using static System.Console;
using static System.Math;
using System;

class main{
	static void Main(){
		//Let's test quad with some definite integrals
		double a=0;
		double b=1;						
		Func<double,double> t1=delegate(double x1){return Sqrt(x1);};
		Func<double,double> t2=delegate(double x2){return 1/Sqrt(x2);};
		Func<double,double> t3=delegate(double x3){return 4*Sqrt(1-x3*x3);};
		Func<double,double> t4=delegate(double x4){return Log(x4)/Sqrt(x4);};
		Func<double,double> gamma=delegate(double x5){return (2/Sqrt(PI))*(Exp(-x5*x5));};

		double r1,r2,r3,r4;

		r1=quad.integrate(t1,a,b);
		r2=quad.integrate(t2,a,b);
		r3=quad.integrate(t3,a,b);
		r4=quad.integrate(t4,a,b);
		Write("We test our quadrature routine with the interesting integrals \n");
		Write("proposed on the homework page: \n");		
		Write("Analytical result , Obtained result \n");
		Write($"I1. {2/3} , {r1} \nI2. 2, {r2} \nI3. {PI} , {r3} \nI4 -4 , {r4} \n\n");
		Write("We use our integrator to implment the error function via its integral\n");
		Write("representation. We make a plot to show if, comparing it with some tabulated \n");
		Write("results. The graphic is shown in plot.png. \n");

		var outfile=new System.IO.StreamWriter("data.txt");
		int maxi=10000;
		double a1=0;
		double b1=3.5;
		double dx=(b1-a1)/(maxi*1.0);
		double x=0;
		outfile.WriteLine("0 0");
		for(int i=0;i<maxi;i++){
			x+=dx;
			outfile.WriteLine($"{x} {quad.integrate(gamma,a1,x)}");}
		outfile.Close();
			
	}





}
