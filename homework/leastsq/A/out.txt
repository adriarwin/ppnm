We have introduced the data from the exercise page in our least square function in order to fit it 
using three diferent functions. f1(x)=1, f2(x)=x, f3(x)=x*x. With our method we have found the coefficients 
by which we can fit our experimental data as a sum of the former functions: f(x)= sum_i f_i(x)*c_i(x). We 
have found the following coefficients:
c_1=4.95658046249803 c_2=-0.174762183414483 c_3=0.000200205204269023

 The c2 coefficient is the decay constant, which can be converted to half-life time.
Our fit predicts a halflife time of the Ra-244 of 3.96623094892337 daysExperimental data says that is 3.66(4) days.