using static System.Console;
using static System.Math;
using System;

class main{
	static void Main(){
		//Function we'll use during the excercise		
		Func<double,double[]> fs = delegate(double z){
			double[] res=new double[3];
			res[0]=1;
			res[1]=z;
			res[2]=z*z;
			return res;};
		double[] uncert=new double[3];
		//Function that returns c coefficients using ours QSR class. 
		Func<double[],double[],double[],Func<double,double[]>,double[]>
		leastsq=delegate(double[] xvec, double[] yvec, double[] dyvec,Func<double,double[]> fset){
			//Size of the matrixs and creation of b and A
			int n=yvec.Length;
			int m=fset(0).Length;
			double[] cont=new double[m];
			matrix A=new matrix(n,m);
			vector b=new vector(n);
			for(int i=0;i<n;i++){
				b[i]=yvec[i]/dyvec[i];
				cont=fset(xvec[i]);
				for(int j=0;j<m;j++){
					A[i,j]=cont[j]/dyvec[i];}
				}
			//We call a QSR class and solve the problem.
			QRGS solver=new QRGS();
			solver.QRGSm(A);
			double[] result=solver.solve(b);
			//Now, we generate a matrix A.transpose*A which we have to invert. That is the covariance matrix.			

			matrix A1=A.transpose()*A;
			QRGS solver1=new QRGS();
			solver1.QRGSm(A1);
			matrix covariance=solver1.inverse();
			for(int i=0;i<m;i++){uncert[i]=Sqrt(covariance[i,i]);}	
			return result;};
		
		//Prepare the data we'll use.
		double[] tdat=new double[9]{1,  2,  3, 4, 6, 9,   10,  13,  15};
		double[] ydat=new double[9]{117,100,88,72,53,29.5,25.2,15.2,11.1};
		double[] dydat=new double[9]{5,5,5,5,5,1,1,1,1};
		//Data manipulation
		for(int k=0;k<tdat.Length;k++){
					dydat[k]=dydat[k]/ydat[k];
					ydat[k]=Log(ydat[k]);}
		
		//We write everything here:
		var outfile=new System.IO.StreamWriter("data.txt");
		//Try the function.
		
		double[] c=leastsq(tdat,ydat,dydat,fs);
		Write($"C coefficients and its uncertainity:\n\n");

		Write($"c_1={c[0]} c_2={c[1]} c_3={c[2]}\n");		
		Write($"dc_1={uncert[0]} dc_2={uncert[1]} dc_3={uncert[2]}\n");			

		Write("\n The c2 coefficient is the decay constant, which can be converted to half-life time.\n");
		Write("\n Now we include the error in the coefficient.\n");
		Write($"Our fit predicts a halflife time of the Ra-244 of {-1*Log(2)/c[1]} days. \n");
		Write($"With the uncertainity: {(+1*Log(2)/(c[1]*c[1]))*uncert[1]} days. \n");
		Write($"Experimental data says that is 3.66(4) days.\n");
		Write($"The fitted halftime is within the experimental current data.\n");
		//Now let us try the function, and that everything works fine. 
		//First we write the data points
		for(int l=0;l<tdat.Length;l++){
				outfile.WriteLine($"{tdat[l]} {ydat[l]} {dydat[l]}");
				}				
		outfile.Write("\n\n");
		//Now we write out our fit.
		double alim=tdat[0];
		double blim=tdat[tdat.Length-1];
		double steps=1000;
		double dx=(blim-alim)/steps;
		double[] cont2;
		for(double xstep=0;xstep<blim;xstep+=dx){
				cont2=fs(xstep);	
				outfile.WriteLine($"{xstep} {cont2[0]*c[0]+cont2[1]*c[1]+cont2[2]*c[2]}");}
		outfile.Close();		
	}





}
