using static System.Console;
using static System.Math;
using System;


public class QRGS{
        public matrix R;
        public matrix Q;
        public void QRGSm(matrix A){
                int m=A.size2;
                Q=A.copy();
                R=new matrix(m,m);
                for(int i=0;i<m;i++){
                        R[i,i]=Q[i].norm();
                        Q[i]/=R[i,i];
                        for(int j=i+1;j<m;j++){
                                R[i,j]=Q[i].dot(Q[j]);
                                Q[j]-=Q[i]*R[i,j];
                                }
                        }
        }
        public vector solve(vector b){
                vector C=Q.transpose()*b;
                for(int i=C.size-1;i>=0;i-=1){
                        double sum=0;
                        for(int j=i+1;j<C.size;j++){sum+=R[i,j]*C[j];}
                        C[i]=(1/R[i,i])*(C[i]-sum);
                        }


                return C;}
        public matrix inverse(){
                int m=Q.size2;
                matrix B=Q.copy();
                vector q1=new vector(m);
                q1.set_zero();
                for(int i=0;i<m;i++){
                        q1[i]=1;
                        B[i]=solve(q1);
                        q1.set_zero();
                        }

                return B;}
}
