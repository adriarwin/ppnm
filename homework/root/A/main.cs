using static System.Console;
using static System.Math;
using System;



class main{
	static void Main(){

		Write("We test our root finding method with two functions. f1(x,y)=(x-3,y-2). And f2=Gradient of Rosenbrock's valley function.\n");
		//Now, let's test this first with the easiest possible function.
		Func<vector,vector> ftest1=delegate(vector xtest1){
					vector rtest1=new vector((xtest1[0]-3)*(xtest1[0]-3),(xtest1[1]-2)*(xtest1[1]-2));
					return rtest1;};			
		
		Write("For f1 we have the following roots \n");
		vector x01=new vector(5,4);
		vector r1=root.newton(ftest1,x01);
		WriteLine($"Our method=({r1[0]},{r1[1]}). Analytical:(3,2) \n ");
		
		Write("For f2 we have the following roots \n");

		//And with the Rosenbrock's valley function.
		Func<vector,vector> ftest2=delegate(vector xtest2){
					double x=xtest2[0];
					double y=xtest2[1];
					vector rtest2=new vector(-2*(1-x)-400*x*(y-x*x), 200*(y-x*x));
					return rtest2;
					};

		vector x02=new vector(5,5);
		vector r2=root.newton(ftest2,x02);
		WriteLine($"Root=({r2[0]},{r2[1]}). Analytical:(1,1)");

	}





}

