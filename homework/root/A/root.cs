using static System.Console;
using static System.Math;
using System;

public class root{
        public static vector newton(Func<vector,vector> f,
        vector x0, double eps=1e-2){
                int n=x0.size;
                vector xvec=x0.copy();
                vector xaux=new vector(n);
                vector xdelta=new vector(n);
                vector dxvec=new vector(n);
                matrix Jmat=new matrix(n,n);
                vector faux=new vector(n);

                double lambda;
                do{
                        //First, let's define the step and the function vector
                        for(int i=0;i<n;i++){dxvec[i]=Abs(xvec[i])*Pow(2,-26);}
                        faux=f(xvec);
                        //Let's create the jacobin matrix.
                        for(int i1=0;i1<n;i1++){
                                        for(int j=0;j<n;j++){
                                                xaux=xvec.copy();
                                                xaux[j]=xaux[j]+dxvec[j];
                                                Jmat[i1,j]=(f(xaux)[i1]-faux[i1])/dxvec[j];};}

                        //Now, let's solve the system Jmat dxvec=-faux using our class
                        QRGS solver=new QRGS();
                        solver.QRGSm(Jmat);
                        xdelta=solver.solve(-faux);

                        //Backtracking
                        lambda=1.0;
                        while(f(xvec+lambda*xdelta).norm()>(1-lambda/2)*faux.norm() && lambda > 1/32){lambda=lambda/2;}
                        xvec=xvec+lambda*xdelta;
                        }
                        while(f(xvec).norm()>eps && xdelta.norm()>dxvec.norm());

                return xvec;

        }
}
