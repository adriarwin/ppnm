using static System.Console;
using static System.Math;
using System;


class main{
	static void Main(){
		//Let's test the jacobi class
		Write("To test our Jacobi class, we generate a random 5x5 symmetric matrix, A, and we diagonalize it. \n");
		Write("With our Jacobi class we generate V and D, which satisfy Vt A V= D. We check \n");
		Write("the following identities using the approx method from the matrix.cs class. If the identities hold \n");
		Write("True is printed after the arrow \n");
		Random rnd=new Random();
		int n=5;
		double epsilon1=0.001;
		matrix Arand=new matrix(n,n);
		for(int i=0;i<n;i++){
			for(int j=i;j<n;j++){
					Arand[i,j]=rnd.Next(1,10);
					Arand[j,i]=Arand[i,j];}
		}
		
		jacobi solver=new jacobi();
		solver.cycles(Arand,epsilon1);								
		//Now, let's do some tests.
		//precision
		double epsilon2=0.0001;
		matrix test1m=((solver.Vmat).transpose())*Arand*(solver.Vmat);
		bool test1b=test1m.approx(solver.Dmat,epsilon2,epsilon2);
		matrix test2m=(solver.Vmat)*solver.Dmat*(solver.Vmat).transpose();
		bool test2b=test2m.approx(Arand,epsilon2,epsilon2);
		matrix testid=matrix.id(n);
		bool test3b=testid.approx((solver.Vmat).transpose()*solver.Vmat, epsilon2, epsilon2);
		bool test4b=testid.approx((solver.Vmat)*(solver.Vmat).transpose(),epsilon2, epsilon2);
		WriteLine($"V^t A V = D --> {test1b}");
		WriteLine($"V D V^t = A --> {test2b}");
		WriteLine($"V^t V = I --> {test3b}");
		WriteLine($"V V^t = I --> {test4b}");		
	}

}
