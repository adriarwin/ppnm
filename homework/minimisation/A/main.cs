using static System.Console;
using static System.Math;
using System;


class main{
	static void Main(){
		Func<vector,double> rbf=delegate(vector xtest1){
				double result1;
				result1=Pow(1-xtest1[0],2)+100*Pow(xtest1[1]-xtest1[0]*xtest1[0],2);
				return result1;};

		Func<vector,double> hbl=delegate(vector xtest2){
				double result2;
				result2=Pow(xtest2[0]*xtest2[0]+xtest2[1]-11,2)+Pow(xtest2[0]+xtest2[1]*xtest2[1]-7,2);
				return result2;};

		Write("Testing the minimisation function.\n");
		Write("Test 1. Rosenbrok function (a=1). Start point=(5,5). Analytical minimum=(1,1)\n");
		Write("Class Results:\n");		
		qnew test1=new qnew();
		vector start1=new vector(5,5);
		vector vectest1=new vector(2);
		double acce=0.01;
		vectest1=test1.qnewton(rbf,start1,acce);
		Write($"Minimum at=({vectest1[0]},{vectest1[1]}). Number of steps:{test1.counter}\n\n");
		
		Write("Test 2. Himmelblau's function. Start point=(4,-1). Analytical minimum=(-3.78,-3.28)\n");
                Write("Class Results:\n");
                qnew test2=new qnew();
                vector start2=new vector(4,-1);
                vector vectest2=new vector(2);
                double acce2=0.001;
                vectest2=test2.qnewton(hbl,start2,acce2);
                Write($"Minimum at=({vectest2[0]},{vectest2[1]}). Number of steps:{test2.counter}\n");

								
	}





}
