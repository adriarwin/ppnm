using static System.Console;
using static System.Math;
using System;


public class qnew{
	public int counter;
	public vector qnewton(Func<vector,double> f, vector start, double acc){
			counter=0;
			int m=start.size;
			matrix B=matrix.id(m);
			vector xvec=start;
			double lambda;
			vector step=new vector(m);
			do{
			step=-B*gradient(f,xvec);
			//Backtracking
			lambda=1.0;
			while(f(xvec+lambda*step)>f(xvec) && lambda > 1/64){lambda=lambda/2;}
				if(lambda>1/64){
					step=lambda*step;
					xvec=xvec+step;
					B=B+onerank(f,xvec,step,B);}
				else{
					step=lambda*step;
					B=matrix.id(m);
					xvec=xvec+step;
					}
			counter+=1;
			}
			while(gradient(f,xvec).norm()>acc);
			return xvec;
		}

	public static vector gradient(Func<vector,double> f, vector xvec){
		int m=xvec.size;
		vector xaux=new vector(m);
		vector result=new vector(m);
		vector dxvec=new vector(m);
		for(int i=0;i<m;i++){
				dxvec[i]=Abs(xvec[i])*Pow(2,-26);
				xaux=xvec.copy();
				xaux[i]+=dxvec[i];
				result[i]=(f(xaux)-f(xvec))/dxvec[i];}
		return result;
		}

	public static matrix onerank(Func<vector,double> f,vector xvec, vector svec, matrix B, double eps=1e-6){
		int m=xvec.size;
		vector y=gradient(f,xvec+svec)-gradient(f,xvec);
		vector u=svec-B*y;
		double prod=u.dot(y);
		matrix result=new matrix(m,m);
		if(prod>eps){result=(1/prod)*matrix.outer(u,u);}
		else{result=matrix.zerom(m);}
		return result;}

}
