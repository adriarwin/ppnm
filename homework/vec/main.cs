using static System.Console;
class main{
	static void Main(){
	//Test class
	Write("Part A: \n");
	vec u=new vec(1,2,3);
	vec v=new vec(100,200,300);
	u.print("u=");
	v.print("v=");
	(u+v).print("u+v = ");
	(u-v).print("u-v = ");
	var tmp=v*5;
	(5*v).print("5*v=");
	tmp.print("v*5=");
	(-u).print("-u=");
	Write("\n");
	Write("Part B: \n");
	double a=vec.dot(u,v);
	double b=vec.norm(u);
	vec xvec=new vec(1,0,0);
	vec yvec=new vec(0,1,0);
	vec zvec=vec.product(xvec,yvec);
	Write($"dot(u,v)={a}\nnorm(u)={b}\n\n");
	xvec.print("x=");
	yvec.print("y=");
	zvec.print("Vector product (x,y)=");
		
	
	}
}

