using static System.Console;
using static System.Math;

public class vec{
	public double x,y,z;
	//constructors:
	public vec(){x=y=z=0;}
	public vec(double a, double b, double c){x=a;y=b;z=c;}
	//operators
	public static vec operator*(vec v, double d){return new vec(d*v.x,d*v.y,d*v.z);}
	public static vec operator*(double d,vec v){return v*d;}
	public static vec operator+(vec u, vec v){return new vec(u.x+v.x,u.y+v.y,u.z+v.z);}
	public static vec operator-(vec u, vec v){return new vec(u.x-v.x,u.y-v.y,u.z-v.z);}
	public static double dot(vec v,vec w){return v.x*w.x+v.y*w.y+v.z*w.z;}
	public static vec operator-(vec u){return new vec(-u.x,-u.y,-u.z);}
	public static double norm(vec v){return Sqrt(v.x*v.x+v.y*v.y+v.z*v.z);}
	public static vec product(vec u, vec v){return new vec(u.y*v.z-u.z*v.y,u.z*v.x-u.x*v.z,u.x*v.y-u.y*v.x);}
	//methods
	public void print(string s){Write(s);Write($"({x},{y},{z})\n");}
	public void print(){this.print("");}
}
