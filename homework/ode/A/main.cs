using static System.Console;
using static System.Math;
using System;

class main{
	static void Main(){
	Write("In this homework we use our RK method to solve two differential equations,Eq1. d2u/dx2=-u \n");
	Write("and Eq2 d2u/dx2+b*du/dx+c*Sin(theta(t))=0 with b=0.25 and c=5.0. We solve them and represent \n");
	Write("their solutions in plot 1 (for Eq1) and plot 2 (for Eq2). We represent u (theta) and du/dx (omega).\n" );
	//Let's test this with two different differential equations
	var outfile = new System.IO.StreamWriter("data.txt");
	//Example 1: u''=-u
	Func<double,vector,vector> u1=delegate(double x1,vector y01){
				vector y1=new vector(y01[1],-y01[0]);
				return y1;};			
	//Example 2: u''+b*u'+c*sin(theta(t))=0. b=0.25, c=5.0
	Func<double,vector,vector> u2=delegate(double x2,vector y02){
				double b=0.25,c=5.0;
				vector y2=new vector(y02[1],-b*y02[1]-c*Sin(y02[0]));
				return y2;};
	//Initial conditions for both and other parameters
	vector y001=new vector(1,0);
	vector y002=new vector(PI-0.1,0);
	double a=0;
	double fi=10;
	double dx=0.02;
	vector ysol;
	//Example 1 loop
	for(double x=a;x<fi;x+=dx){
		ysol=rk12.driver(u1,a,y001,x);
		outfile.WriteLine($"{x} {ysol[0]} {ysol[1]}");
		}
	outfile.Write("\n\n");
	//Example 2 loop
	for(double x=a;x<fi;x+=dx){
		ysol=rk12.driver(u2,a,y002,x);
		outfile.WriteLine($"{x} {ysol[0]} {ysol[1]}");
		}
	outfile.Close();
	
	}





}
