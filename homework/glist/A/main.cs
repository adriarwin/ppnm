using static System.Math;
using static System.Console;
using System;


public class genlist<T>{
	public T[] data;
	public int size{get{return data.Length;}} //property
	public genlist(){data = new T[0]; }
	public void push(T item){
		T[] newdata = new T[size+1];
		for(int i=0;i<size;i++)newdata[i]=data[i];
		newdata[size]=item;
		data=newdata;
	}
}

class main{
	static void Main(){
		var list=new genlist<double[]>();
		char[] delimiters = {' ','\t'};
		var options = StringSplitOptions.RemoveEmptyEntries;
		WriteLine("Numbers from input file written in exponential form");
		for(string line = ReadLine(); line!=null; line = ReadLine()){
			var words = line.Split(delimiters,options);
			int n = words.Length;
			var numbers = new double[n];
			for(int i=0;i<n;i++) numbers[i] = double.Parse(words[i]);
			list.push(numbers);
			}	
		for(int i=0;i<list.size;i++){
			var numbers = list.data[i];
			foreach(var number in numbers)Write($"{number:e}  ");
			WriteLine();
			}
	
	}
}
