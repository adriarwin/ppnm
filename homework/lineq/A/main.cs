using static System.Console;
using static System.Math;
using System;



class main{
	static void Main(){	
		Random rnd = new Random();
		//The excercise starts now after some experiments.
		//Random matrix generation
		int n=6;
		int m=5;
		matrix Arand=new matrix(n,m);
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				Arand[i,j]=rnd.Next(1,10);}
			}
		QRGS grand=new QRGS();
		grand.QRGSm(Arand);
		Write("We have constructed a class QRGS that for a given matrix A, generates a matrix Q and R which satisfy A=QR.\n");
		Write("We have generated a random matrix A of dimensions 6x5 and generated matrix Q and R from it. \n");
		Write("The following relations have been checked by means of the approx method from the matrix class. \n");
		//Checking that R is upper triangular.
		double sumt=0;
		for(int i=0;i<m;i++){
			for(int j=0;j<i;j++){
				sumt+=grand.R[i,j];	
				}
			}
		if(sumt==0){
			WriteLine($"R is upper triangular, sum of the lower triangular={sumt}");}
		//Checking that Q^T Q is equal to the identity.
		matrix tstid=grand.Q.transpose()*grand.Q;
		matrix idr=matrix.id(tstid.size1);
		double epsilon=0.001;
		//Testing if it is the identity:
		bool test2=tstid.approx(idr);
		if(test2==true){WriteLine("Q^t Q = I");}
		else{WriteLine("Q^t Q not I");}
		bool test3=Arand.approx(grand.Q*grand.R);
		if(test3==true){WriteLine("QR=A");}
		else{WriteLine("QR not A");}
					
		//Now, let's test the solve method. Square matrix.
		Write("\n Now we have generated a 6x6 random square matrix, A, and a random vector, b, of dimension 6. Together they \n ");
		Write("make up for a linear system which we can solve with our method. The goal is to find x which satisfies Ax=b \n");
		Write("To do so first we generate Q and R and then we are ready to solve the system. We check with the approx matrix method \n");
		Write("That Ax=b. \n\n");
		matrix Arand2=new matrix(m,m);
		QRGS grand2=new QRGS();
		vector b2=new vector(m);
		for(int i=0;i<m;i++){
                       for(int j=0;j<m;j++){
                                Arand2[i,j]=rnd.Next(1,10);}
                	b2[i]=rnd.Next(1,10);
		        }
		grand2.QRGSm(Arand2);
		vector result2=grand2.solve(b2);
		double acc2=0.1;
		bool test4=vector.approx(Arand2*result2,b2,acc2,acc2);
		if(test4==true){WriteLine("Ax=b");}
		else{WriteLine("Ax not equal to b");}		
				
	}





}
