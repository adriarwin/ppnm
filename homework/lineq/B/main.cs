using static System.Console;
using static System.Math;
using System;



class main{
	static void Main(){
		Write("We generate a random matrix, A, 5x5 and look for its inverse using \n");
		Write("the Q and R matrixes. We check that its inverse, B, satisfy AB=I by \n");
		Write("means of the approx method from the matrix class. \n\n");	
		Random rnd = new Random();
		int m=5;
		//The excercise starts now after some experiments.

		//Now, let's test the solve method. Square matrix.
		matrix Arand2=new matrix(m,m);
		QRGS grand2=new QRGS();

		vector b2=new vector(m);
		for(int i=0;i<m;i++){
                       for(int j=0;j<m;j++){
                                Arand2[i,j]=rnd.Next(1,10);}
		        }
		grand2.QRGSm(Arand2);
		matrix b3=new matrix(m,m);
		b3=grand2.inverse();
		matrix id3=matrix.id(m);
		bool test5=id3.approx(Arand2*b3);
		if(test5==true){WriteLine("AxB=I");}
		else{WriteLine("AxB not I");}		
				
	}





}
