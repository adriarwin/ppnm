using static System.Console;
using static System.Math;
using System;

class main{
	static double erf(double x){
		if(x<0) return -erf(-x);
		double[] a={0.254829592,-0.284496736,1.421413741,-1.453152027,1.061405429};
		double t=1/(1+0.3275911*x);
		double sum=t*(a[0]+t*(a[1]+t*(a[2]+t*(a[3]+t*a[4]))));
		return 1-sum*Exp(-x*x);
	}

	static double gamma(double x){
		if(x<0)return PI/Sin(PI*x)/gamma(1-x);
		if(x<9)return gamma(x+1)/x;
		double lngamma=x*Log(x+1/(12*x-1/x/10))-x+Log(2*PI/x)/2;
		return Exp(lngamma);
	}

	static double log_gamma(double x){
		return x*Log(x+1/(12*x - 1/x/10)) - x+Log(2*PI/x)/2;
	}

	static void Main(){
		Write("We represent the error function (A part of the homework), \n");
		Write("and the gamma function and its logarithm (B part of the homework).\n");
		var outfile=new System.IO.StreamWriter("raw.txt");
		double a=0;
		double b=3.5;
		double N=10000;
		double dx=(b-a)/N;
		for (double k=a; k<=b;k+=dx){
			outfile.WriteLine($"{k} {erf(k)}");
		}
		outfile.Write("\n\n");
		double c=-5;
		double d=5;
		double M=10000;
		double dx2=(d-c)/M;
		for (double l=c; l<=d; l+=dx2){
			outfile.WriteLine($"{l} {gamma(l)}");
			
		}
		outfile.Write("\n\n");
		for (double l=-5; l<=d; l+=dx2){
			if(Double.IsNaN(log_gamma(l))){outfile.WriteLine($"{l} {0}");}
			else{outfile.WriteLine($"{l} {log_gamma(l)}");}
	        }
		outfile.Close();
		
	}

}
