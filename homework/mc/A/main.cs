using static System.Console;
using static System.Math;
using System;

class main{
	static void Main(){
		//Let's test this with our complex function.
		Func<vector,double> fcom=delegate(vector x){
				int n=3;
				double low=0.0;
				double up=1.0;
				double result=0.0;
				for(int i=0;i<n;i++){
					if(x[i]<low || x[i]>up)return result;
					}
				result=1/(1-Cos(x[0]*PI)*Cos(x[1]*PI)*Cos(x[2]*PI));
				return result;}; 
		Func<vector,double> fexp=delegate(vector x1){
                                int n1=2;
                                double low1=0.0;
                                double up1=1.0;
                                double result1=0.0;
                                for(int i=0;i<n1;i++){
                                        if(x1[i]<low1 || x1[i]>up1)return result1;
                                        }
                                result1=Exp(-x1[0]-x1[1]);
                                return result1;};
		Func<vector,double> fvol=delegate(vector x2){
                                int n2=2;
                                double low2=-0.5;
                                double up2=+0.5;
                                double result2=0.0;
                                for(int i=0;i<n2;i++){
                                        if(x2[i]<low2 || x2[i]>up2)return result2;
                                        }
                                result2=x2[0]+x2[1];
                                return result2;};

		//2D integrals.
		Write("We will test our MC plain subrotine with the following integrals. \n");
		Write($"I1. Exp(-x -y) in the limits inf=(0,0) sup=(1,1). Analytical value:{Pow(1-Exp(-1),2)}\n");
		Write("I2. x+y in the limits inf=(-0.5,-0.5), sup=(0.5,0.5). Analytical value: 0 \n");
		Write("I3. Function from the excercise. Analytical value: 1.3932039296856768591842462603255 \n" );
		Write("\nWe get the folowing results with 10000 iteration of our MC plain method: \n");
		int iter=10000;
		vector lowvec1=new vector(-0.1,-0.1);
		vector upvec1=new vector(1.1,1.1);
		(double inte1,double err1)=mc.plainmc(fexp,lowvec1,upvec1,iter);
                WriteLine($"I1 value:{inte1}, Error: {err1}");
		
		vector lowvec2=new vector(0.7,0.7);
		vector upvec2=new vector(-0.7,-0.7);
		(double inte2,double err2)=mc.plainmc(fvol,lowvec2,upvec2,iter);
                WriteLine($"I2 value:{inte2}, Error: {err2}");
		//Upper and lower limits
		vector lowvec=new vector(-0.1,-0.1,-0.1);
		vector upvec=new vector(1.1,1.1,1.1);
		
		(double inte,double err)=mc.plainmc(fcom,lowvec,upvec,iter);
		WriteLine($"I3 value:{inte}, Error: {err}");
		}





}
