using static System.Console;
using static System.Math;
using System;


public class qspline{
        public double [] x,y,b,c;
        public void qsp(double[] xs,double[] ys){
                x=xs;
                y=ys;
                int n=x.Length;
                b=new double[n-1];
                c=new double[n-1];
                double[] h=new double[n-1];
                double[] p=new double[n-1];
                for(int i=0;i<n-1;i++){
                        h[i]=x[i+1]-x[i];
                        p[i]=(y[i+1]-y[i])/h[i];
                        }
                c[0]=0;
                for(int m=1;m<=n-2;m++){
                        c[m]=(1/h[m])*(p[m]-p[m-1]-c[m-1]*h[m-1]);
                        }
                c[n-2]/=2;
                for(int s=n-3;s>=0;s--){
                        c[s]=(1/h[s])*(p[s+1]-c[s+1]*h[s+1]-p[s]);
                        }
                for(int t=0;t<=n-2;t++){
                        b[t]=p[t]-c[t]*h[t];
                        }
                }
        public double sp(double z){
                        int i=binsearch(x,z);
                        return y[i]+b[i]*(z-x[i])+c[i]*(z-x[i])*(z-x[i]);
                        }
        public static int binsearch(double[] x, double z)
        {/* locates the interval for z by bisection */
        if(!(x[0]<=z && z<=x[x.Length-1])) throw new Exception("binsearch: bad z");
        int i=0, j=x.Length-1;
        while(j-i>1){
                int mid=(i+j)/2;
                if(z>x[mid]) i=mid; else j=mid;
                }
        return i;
        }
        public double derivative(double z){
                int i=binsearch(x,z);
                return b[i]+2*c[i]*(z-x[i]);
                }
        public double integral(double z){
                int i=binsearch(x,z);
                double result=0;
                for(int n=0;n<i;n++){
                        result+=y[n]*(x[n+1]-x[n])+b[n]*0.5*Pow((x[n+1]-x[n]),2)+(c[n]/3)*Pow((x[n+1]-x[n]),3);
                        }
                if (!(i==x.Length-1)){
                        result+=y[i]*(z-x[i])+b[i]*0.5*Pow(z-x[i],2)+(c[i]/3)*Pow(z-x[i],3);
                        }
                return result;
                }
}
