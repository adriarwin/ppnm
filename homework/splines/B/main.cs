using static System.Console;
using static System.Math;
using System;

class main{
	static void Main(){
		
		Write("Final result part B. We make use of the following functions: f1(x)=1,f2(x)=x,f3(x)=x*x\n");
		Write("For each of them we use 5 points (1 to 5, dx=1) which we use for interpolating the functions.\n");
		Write("The quadrataic spline coefficients b and c can be calculated analliticaly for each of the 4 intrvals. \n ");
		Write("We compare those with the results from our own quadratic spline class.\n ");		
		Write("Expected results:\n");
		Write("f1--> b=0,0,0,0 f2 --> b=1,1,1,1  f3 --> b=2,4,6,8 \n");
		Write("f1--> c=0,0,0,0 f2 --> c=0,0,0,0  f3 --> c=1,1,1,1 \n");
				
		double [] xb=new double[5];
		double [] yb0=new double[5];
		double [] yb1=new double[5];
		double [] yb2=new double[5];
		var outfile0 = new System.IO.StreamWriter("tabulated.txt");
		for(int s=0;s<5;s++){
			xb[s]=s+1;
			yb0[s]=1;
			yb1[s]=s+1;
			yb2[s]=(s+1)*(s+1);	
			outfile0.WriteLine($"{xb[s]} {yb0[s]} {yb1[s]} {yb2[s]}");
		}
		outfile0.Close();
		qspline f0=new qspline();
		qspline f1=new qspline();
		qspline f2=new qspline();
		f0.qsp(xb,yb0);
		f1.qsp(xb,yb1);
		f2.qsp(xb,yb2);
		Write("Class results:\n");
                Write($"f1--> b={f0.b[0]},{f0.b[1]},{f0.b[2]},{f0.b[3]} f2 --> b={f1.b[0]},{f1.b[1]},{f1.b[2]},{f1.b[3]}  f3 --> b={f2.b[0]},{f2.b[1]},{f2.b[2]},{f2.b[3]} \n");
		Write($"f1--> c={f0.c[0]},{f0.c[1]},{f0.c[2]},{f0.c[3]} f2 --> c={f1.c[0]},{f1.c[1]},{f1.c[2]},{f1.c[3]}  f3 --> c={f2.c[0]},{f2.c[1]},{f2.c[2]},{f2.c[3]}\n");
		Write("\n\n We will also try the derivative and integral functions of our subroutine comparing \n");
		Write("the result given by our subroutine and the analytical one.");

		double a=1.0001;
		double b=4.9999;
		int N=10000;
		double dx=(b-a)/(N*1.0);
		var outfile1 = new System.IO.StreamWriter("der.txt");
		var outfile2 = new System.IO.StreamWriter("int.txt");
		var outfile3 = new System.IO.StreamWriter("values.txt");
		for(double x=a;x<b;x+=dx){
			outfile1.WriteLine($"{x} {f0.derivative(x)} {f1.derivative(x)} {f2.derivative(x)}");
			outfile2.WriteLine($"{x} {f0.integral(x)} {f1.integral(x)} {f2.integral(x)}");
			outfile3.WriteLine($"{x} {f0.sp(x)} {f1.sp(x)} {f2.sp(x)}");
			} 
		outfile1.Close();
		outfile2.Close();
		outfile3.Close();
		}
}

