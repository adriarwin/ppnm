using static System.Console;
using static System.Math;
using System;



class main{
	public static double linterp(double[] x, double[] y, double z){
        int i=binsearch(x,z);
        double dx=x[i+1]-x[i]; if(!(dx>0)) throw new Exception("uups...");
        double dy=y[i+1]-y[i];
        return y[i]+dy/dx*(z-x[i]);
        }

	public static int binsearch(double[] x, double z)
	{/* locates the interval for z by bisection */ 
	if(!(x[0]<=z && z<=x[x.Length-1])) throw new Exception("binsearch: bad z");
	int i=0, j=x.Length-1;
	while(j-i>1){
		int mid=(i+j)/2;
		if(z>x[mid]) i=mid; else j=mid;
		}
	return i;
	}
	public static double linterpInteg(double[] x,double[] y,double z){
		int i=binsearch(x,z);
		double result=0;
		for(int n=0;n<i;n++){
			double dx=x[n+1]-x[n]; if(!(dx>0)) throw new Exception("uups...");
                        double dy=y[n+1]-y[n];
			double dydx=dy/dx;
			result+=y[n]*(x[n+1]-x[n])+dydx*0.5*(x[n+1]-x[n])*(x[n+1]-x[n]);
			}
		if(!(i==(x.Length-1))){
		double dz=z-x[i]; 
                double dt=y[i+1]-y[i];
                double dzdt=dz/dt;
		result+=y[i]*(z-x[i])+dzdt*0.5*(z-x[i])*(z-x[i]);}
		return result;
		}
	static void Main(){
		Write("We interpolate two functions, f(x)=x and f(x)=x*x. We calculate \n");
		Write("their integral with the interpolation. Representation of it can be found \n");
		Write("in the two graphs.");
		var outfile = new System.IO.StreamWriter("data.txt");		
		//Let's test this.
		//Point's we prepare.
		int lim=6;
		double [] x=new double[lim];
		double [] y1=new double[lim];
		double [] y2=new double[lim];
		for(int i=0;i<lim;i++){
			outfile.WriteLine($"{i} {i} {i*i}");
			x[i]=i;
			y1[i]=i;
                        y2[i]=i*i;                 
			}
		outfile.Write("\n\n");
		double N=100;
		double a=x[0];
		double b=x[lim-1];
		double dz=(b-a)/N;
		for(double z=a;z<b;z+=dz){
			outfile.WriteLine($"{z} {linterp(x,y1,z)} {linterp(x,y2,z)} {linterpInteg(x,y1,z)} {linterpInteg(x,y2,z)}");
			}
		outfile.Close();
		}
}

